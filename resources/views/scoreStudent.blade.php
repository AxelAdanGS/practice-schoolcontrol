<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Control Escolar | Registro de Calificaciones</title>
    <link rel="shortcut icon" type="image/jpg" href="https://www.flaticon.com/svg/vstatic/svg/4039/4039112.svg?token=exp=1617167989~hmac=04ad3ec27fd8fd85d6a32cd473042cb7">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">
            <img src="https://www.flaticon.es/svg/vstatic/svg/2490/2490421.svg?token=exp=1617692875~hmac=e594566243dc597ea34b77978213cce2" width="30" height="30" alt="Logo">
        </a>
        <a class="navbar-brand">Control Escolar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('scoreStudent') }}">Registrar Calificaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('checkScore') }}">Consultar Calificaciones</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('signOff') }}">Cerrar Sesión</a>
                </li>
            </ul>
        </div>
    </nav>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5">
                    <div class="login-wrapper my-auto">
                        <div class="text-center">
                            <img src="https://www.flaticon.es/svg/vstatic/svg/1087/1087195.svg?token=exp=1617692758~hmac=bb32f943b6835f6b488b3c158cfdeed1" alt="Icono Calificaciones" width="100" height="100">
                            <h3 class="login-title">Registro de Calificaciones</h3>
                        </div>
                        <div class="col text-center">
                            @if(isset($estatus))
                                @if($estatus == "success")
                                    <h5 class="text-success">{{$mensaje}}</h5>
                                @elseif($estatus == "error")
                                    <h5 class="text-warning">{{$mensaje}}</h5>
                                @endif
                            @endif
                        </div>
                        <form class="user" action="{{route('scoreStudent.form')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group mb-4">
                                <label for="ingles">Inglés V:</label>
                                <input type="number" step="any" id="ingles" name="ingles" min="0.0" max="10.0">
                            </div>
                            <div class="form-group mb-4">
                                <label for="etica">Ética Profesional:</label>
                                <input type="number" step="any" id="etica" name="etica" min="0.0" max="10.0">
                            </div>
                            <div class="form-group mb-4">
                                <label for="matematicas">Matemáticas para la Ingeniería II:</label>
                                <input type="number" step="any" id="matematicas" name="matematicas" min="0.0" max="10.0">
                            </div>
                            <div class="form-group mb-4">
                                <label for="programacion">Programación Cliente/Servidor:</label>
                                <input type="number" step="any" id="programacion" name="programacion" min="0.0" max="10.0">
                            </div>
                            <div class="form-group mb-4">
                                <label for="redes">Fundamentos de Redes:</label>
                                <input type="number" step="any" id="redes" name="redes" min="0.0" max="10.0">
                            </div>
                            <div class="form-group mb-4">
                                <label for="arquitectura">Arquitectura de Software:</label>
                                <input type="number" step="any" id="arquitectura" name="arquitectura" min="0.0" max="10.0">
                            </div>
                            <div class="form-group mb-4">
                                <label for="sistemas">Sistemas Digitales:</label>
                                <input type="number" step="any" id="sistemas" name="sistemas" min="0.0" max="10.0">
                            </div>
                            <input name="scoreStudent" id="scoreStudent" class="btn btn-user btn-block btn-primary" type="submit" value="Subir Calificaciones">
                            @if(isset($_GET["r"]))
                                <input type="s" name="url" value="{{$_GET["r"]}}">
                            @endif
                        </form>
                    </div>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </main>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</body>
</html>
