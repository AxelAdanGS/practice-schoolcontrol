<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Control Escolar | Registro</title>
    <link rel="shortcut icon" type="image/jpg" href="https://www.flaticon.com/svg/vstatic/svg/4039/4039112.svg?token=exp=1617167989~hmac=04ad3ec27fd8fd85d6a32cd473042cb7">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5">
                    <div class="text-center">
                        <h1 class="login-title text-center">Creación de Cuenta</h1>
                        <i>Para la creación de cuenta de un alumno, ingrese información veridica</i>
                        <hr>
                    </div>

                    <form class="user" method="post" action="{{route('register.form')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control form-control-user" placeholder="Nombre(s)">
                        </div>
                        <div class="form-group">
                            <input type="text" name="paternalSurname" id="paternalSurname" class="form-control form-control-user" placeholder="Apellido Paterno">
                        </div>
                        <div class="form-group">
                            <input type="text" name="maternalSurname" id="maternalSurname" class="form-control form-control-user" placeholder="Apellido Materno">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control form-control-user" id="email" placeholder="Correo Electrónico" name="email">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" name="password1" id="password1" class="form-control form-control-user" placeholder="Contraseña">
                            </div>
                            <div class="col-sm-6">
                                <input type="password" name="password2" id="password2" class="form-control form-control-user" placeholder="Repetir Contraseña">
                            </div>
                        </div>
                        <input type="submit" class="btn btn-block btn-dark" value="Crear Cuenta">
                        <div class="text-center">
                            <label class="text-danger">
                                @if(isset($estatus))
                                    <h5 class="text-danger">{{$mensaje}}</h5>
                                @endif
                            </label>
                        </div>
                    </form>

                    <hr>

                    <div class="text-center">
                        <p class="login-wrapper-footer-text">¿Tiene una cuenta? <a href="{{route('login')}}"class="text-reset">Inicie Sesión</a></p>
                    </div>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </main>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</body>
</html>
