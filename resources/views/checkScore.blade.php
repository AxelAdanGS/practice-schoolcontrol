<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Control Escolar | Consulta de Calificaciones</title>
    <link rel="shortcut icon" type="image/jpg" href="https://www.flaticon.com/svg/vstatic/svg/4039/4039112.svg?token=exp=1617167989~hmac=04ad3ec27fd8fd85d6a32cd473042cb7">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.0.2/chart.js" integrity="sha512-n8DscwKN6+Yjr7rI6mL+m9nS4uCEgIrKRFcP0EOkIvzOLUyQgOjWK15hRfoCJQZe0s6XrARyXjpvGFo1w9N3xg==" crossorigin="anonymous"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">
            <img src="resources/images/loginLogo.svg" width="30" height="30" alt="Logo">
        </a>
        <a class="navbar-brand">Control Escolar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('scoreStudent') }}">Registrar Calificaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('checkScore') }}">Consultar Calificaciones</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('signOff') }}">Cerrar Sesión</a>
                </li>
            </ul>
        </div>
    </nav>
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5">
                    <div class="login-wrapper my-auto">
                        <div class="text-center">
                            <h3 class="login-title">Calificaciones</h3>
                        </div>
                        <div class="col text-center">
                            @if(isset($estatus))
                                @if($estatus == "success")
                                    <h5 class="text-success">{{$mensaje}}</h5>
                                @elseif($estatus == "error")
                                    <h5 class="text-warning">{{$mensaje}}</h5>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </main>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5">
                    <canvas id="myChart" width="400" height="400"></canvas>
                    <script>
                        var ctx = document.getElementById('myChart').getContext('2d');
                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: ['Ingles', 'Etica Profesional', 'Matematicas para la Ingenieria', 'Programacion Cliente/Servidor', 'Redes', 'Arquitectura de Software', 'Sistemas Digitales'],
                                datasets: [{
                                    label: 'Calificaciones',
                                    data: [<?php
                                        foreach ($datos as $data){
                                            echo $datos->ingles;
                                            echo $datos->etica;
                                            echo $datos->matematicas;
                                            echo $datos->programacion;
                                            echo $datos->redes;
                                            echo $datos->arquitectura;
                                            echo $datos->sistemas;
                                        }
                                    ?>
                                    ],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    y: {
                                        beginAtZero: true
                                    }
                                }
                            }
                        });

                        function cargarGrafico() {
                            $.ajax({
                                url: 'app/Console/UsuariosController.php',
                                type: 'POST'
                            }).done(function (resp) {
                                var data = JSON.parse(resp);
                                for (var i = 0; i < data.length; i++) {
                                    alert(data[i][1]);
                                }
                            })
                        }
                    </script>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5">
                    <a href="{{route('descargarPDF')}}" class="btn btn-user btn-block btn-primary">Descargar PDF</a>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script></body>
</html>
