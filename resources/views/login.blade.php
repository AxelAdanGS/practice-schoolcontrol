<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Control Escolar | Inicio de Sesion</title>
    <link rel="shortcut icon" type="image/jpg" href="https://www.flaticon.com/svg/vstatic/svg/4039/4039112.svg?token=exp=1617167989~hmac=04ad3ec27fd8fd85d6a32cd473042cb7">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5">
                    <div class="login-wrapper my-auto">
                        <h1 class="login-title text-center">Inicio de Sesión</h1>
                        <div class="col text-center">
                            @if(isset($estatus))
                                @if($estatus == "success")
                                    <h5 class="text-success">{{$mensaje}}</h5>
                                @elseif($estatus == "error")
                                    <h5 class="text-warning">{{$mensaje}}</h5>
                                @endif
                            @endif
                        </div>
                        <form class="user" action="{{route('login.form')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="email">Correo Electrónico</label>
                                <input type="email" name="email" id="email" class="form-control form-control-user" placeholder="correoelectronico@ejemplo.com" required>
                            </div>
                            <div class="form-group mb-4">
                                <label for="password">Contraseña</label>
                                <input type="password" name="password" id="password" class="form-control form-control-user" placeholder="Introduzca su contraseña" required>
                            </div>
                            <input name="login" id="login" class="btn btn-user btn-block btn-dark" type="submit" value="Iniciar Sesión">
                            @if(isset($_GET["r"]))
                                <input type="hidden" name="url" value="{{$_GET["r"]}}">
                            @endif
                        </form>

                        <hr>

                        <div class="text-center">
                            <p class="login-wrapper-footer-text">¿No tiene una cuenta? <a href="{{route('register')}}" class="text-reset">Registrese aquí</a></p>
                        </div>
                        </div>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</body>
</html>
