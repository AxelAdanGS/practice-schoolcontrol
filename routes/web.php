<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/inicio-de-sesion',[\App\Console\UsuariosController::class,'login'])->name('login');
Route::post('/inicio-de-sesion',[\App\Console\UsuariosController::class,'verifyCredentials'])->name('login.form');
Route::get('/cerrar-sesion',[\App\Console\UsuariosController::class,'signOff'])->name('signOff');
Route::get('/registro',[\App\Console\UsuariosController::class,'register'])->name('register');
Route::post('/registro',[\App\Console\UsuariosController::class,'registerForm'])->name('register.form');

Route::prefix('/usuario')->middleware('VerifyUser')->group(function (){
    Route::get('/inicio', [\App\Console\UsuariosController::class, 'home'])->name('home');
    Route::post('/inicio',[\App\Console\UsuariosController::class,'registerDocuments'])->name('registerDocuments.form');
    Route::get('/inicio/registro-calificaciones', [\App\Console\UsuariosController::class, 'scoreStudent'])->name('scoreStudent');
    Route::post('/inicio/registro-calificaciones',[\App\Console\UsuariosController::class,'registerScore'])->name('scoreStudent.form');
    Route::get('/inicio/consulta-calificaciones', [\App\Console\UsuariosController::class, 'checkScore'])->name('checkScore');
    Route::post('/inicio/consulta-calificaciones',[\App\Console\UsuariosController::class,'checkScore'])->name('checkScore.form');
    Route::get('/inicio/boleta-de-calificaciones', '\App\Http\Controllers\PDFController@PDF')->name('descargarPDF');

    //Route::get('/menu',[UsuarioController::class,'menu'])->name('usuario.menu');
    //Route::get('/inicio',[UsuariosController::class,'home'])->name('home');
});
